import Vue from 'vue';
import VueWebsocket from 'vue-websocket';
import * as VueGoogleMaps from 'vue2-google-maps';

import App from './App.vue';
import './registerServiceWorker';

Vue.use(VueGoogleMaps, {
  load: {
    key: 'AIzaSyAR8SnfTEbjDcUcpkJpZ1AYJELXGOc62-E',
    libraries: 'places',
  },
});

Vue.config.productionTip = false;

Vue.use(VueWebsocket, 'wss://bus-server.aeisep.pt/');

new Vue({
  render: h => h(App),
}).$mount('#app');
